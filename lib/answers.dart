import 'package:flutter/material.dart';

class Answers extends StatelessWidget {
  Function function;
  String text;
  Answers({this.function, this.text});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: function,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(10, 10, 50, 10),
        child: Container(
          width: double.infinity,
          height: 30,
          color: Colors.blue,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              text,
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
