import 'package:flutter/material.dart';
import 'package:flutter_application_1/answers.dart';
import 'package:flutter_application_1/question.dart';

void main() => runApp(MaterialApp(
      home: MyApp(),
    ));

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int page = 0;

  void checkNSetNext() {
    if (page + 1 < questions.length) {
      setState(() {
        page++;
      });
    }
  }

  List<String> questions = [
    'What is our favorite animal?',
    'What is our favorite color?',
    'What is our favorite sports game?',
  ];
  List<List> answers = [
    [
      'Dog',
      'Cat',
      'Mouse',
      'Horse',
    ],
    [
      'Yellow',
      'Blue',
      'Green',
      'Red',
    ],
    [
      'Basketball',
      'Tennis',
      'Golf',
      'Football',
    ]
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('QuizApp'),
      ),
      body: Center(
        child: Column(
          children: [
            Question(
              text: questions[page],
            ),
            Answers(
              text: answers[page][0],
              function: checkNSetNext,
            ),
            Answers(
              text: answers[page][1],
              function: checkNSetNext,
            ),
            Answers(
              text: answers[page][2],
              function: checkNSetNext,
            ),
            Answers(
              text: answers[page][3],
              function: checkNSetNext,
            ),
            Image.network(
              'https://cdn-media-1.freecodecamp.org/images/0*ngXgBNNdx6iiWP8q.png',
              width: double.infinity,
            )
          ],
        ),
      ),
    );
  }
}
